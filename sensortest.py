#!/usr/bin/env python3
from ev3dev.ev3 import *
from time import sleep

# Connect EV3 color sensor
cl = ColorSensor()
max = 0
# Put the color sensor into COL-REFLECT mode
# to measure reflected light intensity.
# In this mode the sensor will return a value between 0 and 100
cl.mode='COL-REFLECT'

while True:
        sensor=cl.value()
        print(str(sensor) + ", max"+str(max))
        if (sensor>max):
            max=sensor
        if (sensor>=10):
            cl.mode='COL-AMBIENT'
            print ("woot")
            sleep(20)
            print ("return")
            cl.mode='COL-REFLECT'
        sleep(0.5)
# I get max 80 with white paper, 3mm separation
# and 5 with black plastic, same separation
