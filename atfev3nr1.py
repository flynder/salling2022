#!/usr/bin/env python3
from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
# insert at position 1 in the path, as 0 is the path of this file.
sys.path.insert(1, '/home/robot/')

import config
client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)

#Uppertrain on A

pA = ev3.LegoPort('outA')
pB = ev3.LegoPort('outB')
#pC = ev3.LegoPort('outC')

assert pA.connected
assert pB.connected
#assert pC.connected
pA.mode = 'dc-motor'
pB.mode = 'dc-motor'
#pC.mode = 'dc-motor'

time.sleep(1)
mA = ev3.DcMotor('outA')
mB = ev3.DcMotor('outB')
#mC = ev3.DcMotor('outC')


#print ("run train")
trainruntime = 20
#mC.run_direct(duty_cycle_sp=40)
#time.sleep(trainruntime)
#mC.run_direct(duty_cycle_sp=-40)
#time.sleep(trainruntime)
#mC.run_direct(duty_cycle_sp=0)



# This is the Subscriber

def on_connect(client, userdata, flags, rc):
  client.subscribe("sallingaarhus/command", qos=1)

def on_message(client, userdata, msg):
    global trainruntime
    if msg.payload.decode() == "uppertrain":
      status = "Upper Train Started for " + str(trainruntime) + " seconds"
      client.publish("sallingaarhus/status", status, qos=1);
      print ("upper train run")
      mA.run_direct(duty_cycle_sp=60)
      time.sleep(trainruntime)
      mA.stop()
      mA.run_direct(duty_cycle_sp=-0)
      print ("upper train return")
      mA.run_direct(duty_cycle_sp=-60)
      time.sleep(trainruntime)
      mA.stop()
      mA.run_direct(duty_cycle_sp=-0)
      client.publish("sallingaarhus/status", "Upper Train Stopped", qos=1);
    if msg.payload.decode() == "lowertrain":
      status = "Lower Train Started for " + str(trainruntime) + " seconds"
      client.publish("sallingaarhus/status", status, qos=1);
      print ("lower train run")
      mB.run_direct(duty_cycle_sp=60)
      time.sleep(trainruntime)
      mB.stop()
      mB.run_direct(duty_cycle_sp=-0)
      print ("lower train return")
      mB.run_direct(duty_cycle_sp=-60)
      time.sleep(trainruntime)
      mB.stop()
      mB.run_direct(duty_cycle_sp=-0)
      client.publish("sallingaarhus/status", "Upper Train Stopped", qos=1);


client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
