#!/usr/bin/env python3
from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
import time
# insert at position 1 in the path, as 0 is the path of this file.
sys.path.insert(1, '/home/robot/')

import config
client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)
client.loop_start()

# Connect EV3 color sensor
cl = ColorSensor()
max = 0
# Put the color sensor into COL-REFLECT mode
# to measure reflected light intensity.
# In this mode the sensor will return a value between 0 and 100
cl.mode='COL-REFLECT'

status = "activateupper started"
client.publish("salling2022/status", status, qos=1);


while True:
        sensor=cl.value()
        print(str(sensor) + ", max"+str(max))
        if (sensor>max):
            max=sensor
        if (sensor>=6):
            status = "Upper Train Activated"
            client.publish("salling2022/event/uppertrain", status, qos=1);
            command = "activateupper"
            client.publish("salling2022/command", command, qos=1);
            cl.mode='COL-AMBIENT'
            print ("Upper Train Activated, Sensor mode shifted")
            time.sleep(20)
            print ("Sensor mode return")
            cl.mode='COL-REFLECT'
            max=0
        time.sleep(0.5)
# I get max 80 with white paper, 3mm separation
# and 5 with black plastic, same separation

