from random import randrange
import time
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config

runtime = 120
idle = 5


client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,20)

import datetime as dt
h =  dt.datetime.now().hour + 2

while True:
    while True:
      x=(randrange(3))
      if x == 0:
         print ("tree on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,20)
         client.publish("topic/commands", "tree on", qos=0);
         client.disconnect();
         time.sleep(runtime)
         print ("tree off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,20)
         client.publish("topic/commands", "tree off", qos=0);
         client.disconnect();
         time.sleep(idle)
      elif x == 1:
         print ("police on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,20)
         client.publish("topic/commands", "police on", qos=0);
         client.disconnect();
         time.sleep(runtime)
         print ("police off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,20)
         client.publish("topic/commands", "police off", qos=0);
         client.disconnect();
         time.sleep(idle)         
      elif x == 2:
         print ("caves on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,20)
         client.publish("topic/commands", "caves on", qos=0);
         client.disconnect();
         time.sleep(runtime)
         print ("caves off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,20)
         client.publish("topic/commands", "caves off", qos=0);
         client.disconnect();
         time.sleep(idle)
