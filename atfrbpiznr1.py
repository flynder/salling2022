#!/usr/bin/env python3
#import board
import time
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config
import requests
import json
import os
import datetime as dt



train=0

commands = "sallingaarhus/command";
events = "sallingaarhus/status";
def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe(commands, qos=0)
   print ("Lights ready");
   client.publish(events, "Lights connected", qos=0);

def on_message(client, userdata, msg):
   global train
   global h
   print("Current hour ",  dt.datetime.now().hour)
   print("Current minute ",  dt.datetime.now().minute)
   if msg.payload.decode() == "policelights":
      print("PoliceLights turned on")
      client.publish(events, "PoliceLights turned on", qos=1); 
      os.system("/home/pi/sallingaarhus/hs100.sh -i 192.168.0.51 on")
      time.sleep(20)
      print("PoliceLights turned off")
      client.publish(events, "PoliceLights turned off", qos=1); 
      os.system("/home/pi/sallingaarhus/hs100.sh -i 192.168.0.51 off")
   if msg.payload.decode() == "towlights":
      print("TowLights turned on")
      client.publish(events, "TowLights turned on", qos=1); 
      os.system("/home/pi/sallingaarhus/hs100.sh -i 192.168.0.52 on")
      time.sleep(20)
      print("TowLights turned off")
      client.publish(events, "TowLights turned off", qos=1); 
      os.system("/home/pi/sallingaarhus/hs100.sh -i 192.168.0.52 off")

      

      
      
client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,20)


client.on_connect = on_connect
client.on_message = on_message

client.loop_start()

while True:
   h =  dt.datetime.now().hour
   time.sleep(1)
   
