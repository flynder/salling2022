from random import randrange
import time
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config

daysleep = 600
nightsleep = 60 
soff = 0

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)
client.loop_start()
import datetime as dt
h =  dt.datetime.now().hour + 2



def Off():
    global soff
    if (soff != 1):
        print ("lights off")
        for var in list(range(6)):
            value = var + 1
            command1="salling2022/set/lights/Hue lightstrip plus "+ str(value) +"/"
            param1="0"
            client.publish(command1,param1, qos=0);
        print ("caves off")
        command="salling2022/event/caves"
        param="lightsoff"
        client.publish(command,param, qos=0);
        soff = 1;
        print ("minecraft off")
        client.publish("salling2022/commands/minecraft/lightsoff", "", qos=0);

def Day():
    global soff
    for var in list(range(6)):
        value = var + 1
        command1="salling2022/set/lights/Hue lightstrip plus "+ str(value) +"/"
        param1='{"on":true,"bri":254,"ct":160,"transitiontime":150}'
        client.publish(command1,param1, qos=0);
    print ("lights day")
    time.sleep(daysleep);
    if (soff==1):
            print ("caves on")
            command="salling2022/event/caves"
            param="lightson"
            client.publish(command,param, qos=0);
            print ("minecraft on")
            client.publish("salling2022/commands/minecraft/lightson", "", qos=0);

    soff = 0;
    

def Night():
    print ("lights night");
    for var in list(range(6)):
        value = var + 1
        command1="salling2022/set/lights/Hue lightstrip plus "+ str(value) +"/"
        param1='{"on":true,"bri":50,"ct":153,"transitiontime":150}'
        client.publish(command1,param1, qos=0);
    time.sleep(nightsleep);
   
    
while True:
    h =  dt.datetime.now().hour + 0
    if ((h>=7) and (h<=21)):
         Day()
         #Night()
    else:
         Off()
