#!/usr/bin/env python3
import time
import brickpi3 #import BrickPi3.py file to use BrickPi3 operations
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config

BP = brickpi3.BrickPi3()
out1=-30
out2=50
out3=-50




def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe("topic/commands", qos=1)

def on_message(client, userdata, msg):
   if msg.payload.decode() == "train1 on":
      print("train1 on")
      BP.set_motor_power(BP.PORT_A, out1)
      status = "Train1 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train1 off":
      print("train1 off")
      BP.set_motor_power(BP.PORT_A, 0)
      client.publish("topic/status", "Train1 Stopped", qos=1);   
   if msg.payload.decode() == "train2 on":
      print("train2 on")
      BP.set_motor_power(BP.PORT_B, out2)
      status = "Train2 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train2 off":
      print("train2 off")
      BP.set_motor_power(BP.PORT_B, 0)      
      client.publish("topic/status", "Train2 Stopped", qos=1);   
   if msg.payload.decode() == "train3 on":
      print("train3 on")
      BP.set_motor_power(BP.PORT_C, out3)
      status = "Train3 Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "train3 off":
      print("train3 off")
      BP.set_motor_power(BP.PORT_C, 0)
      client.publish("topic/status", "Train3 Stopped", qos=1);   

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)

time.sleep(0.5)

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
