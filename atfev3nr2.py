#!/usr/bin/env python3
import time
from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config
import os




def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe("sallingaarhus/command", qos=1)

def on_message(client, userdata, msg):
   if msg.payload.decode() == "aroscrane":
      print("Aros Crane on")
      status = "Aros Crane on"
      client.publish("sallingaarhus/status", status, qos=1);
      os.system('/bin/bash -c "/home/robot/sallingaarhus/kran.sh"')
      print("Aros Crane returned")
      status = "Aros Crane returned"
      client.publish("sallingaarhus/status", status, qos=1);


client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)

time.sleep(0.5)

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
