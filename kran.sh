#!/bin/bash
echo -500 > /sys/class/tacho-motor/motor0/speed_sp
run-for-time() { echo run-forever > /sys/class/tacho-motor/motor0/command; sleep $1; echo stop > /sys/class/tacho-motor/motor0/command; }
run-for-time 10
sleep 10
echo 500  > /sys/class/tacho-motor/motor0/speed_sp
run-for-time() { echo run-forever > /sys/class/tacho-motor/motor0/command; sleep $1; echo stop > /sys/class/tacho-motor/motor0/command; }
run-for-time 10
