#!/usr/bin/env python3

import paho.mqtt.client as mqtt
import socket
import sys
# insert at position 1 in the path, as 0 is the path of this file.
sys.path.insert(1, '/home/robot/')
import config

# This is the Publisher

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)
#ip_address = socket.gethostbyname(socket.gethostname())
#text = socket.gethostname() + "is alive, " + ip_address
text = socket.gethostname() + " is alive"
topic = "sallingaarhus/status/" + socket.gethostname()
client.publish(topic, text, qos=1);
client.disconnect();
