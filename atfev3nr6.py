from ev3dev.ev3 import *
import ev3dev.ev3 as ev3
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config


# initial setup of motor ports

p = ev3.LegoPort('outA')
assert p.connected
p.mode = 'dc-motor'
m = ev3.DcMotor('outA')
pC = ev3.LegoPort('outC')
assert pC.connected
pC.mode = 'dc-motor'
mC = ev3.DcMotor('outC')
pD = ev3.LegoPort('outD')
assert pD.connected
pD.mode = 'dc-motor'
mD = ev3.DcMotor('outD')
pB = ev3.LegoPort('outB')
assert pB.connected
pB.mode = 'dc-motor'
mB = ev3.DcMotor('outB')
time.sleep(2)

def on_connect(client, userdata, flags, rc):
   print("Connected with result code "+str(rc))
   client.subscribe("topic/commands", qos=0)

def on_message(client, userdata, msg):
   if msg.payload.decode() == "santa on":
      print("santa on")
      m.run_direct(duty_cycle_sp=40)
      #santa
      mB.run_direct(duty_cycle_sp=40)
      #belt
      mC.run_direct(duty_cycle_sp=-60)
      #elf right
      mD.run_direct(duty_cycle_sp=40)
      status = "Santa Started"
      client.publish("topic/status", status, qos=1);
   if msg.payload.decode() == "santa off":
      print("santa off")
      m.stop
      m.run_direct(duty_cycle_sp=0)
      mB.run_direct(duty_cycle_sp=0)
      mC.run_direct(duty_cycle_sp=0)
      mD.run_direct(duty_cycle_sp=0)
      client.publish("topic/status", "Santa Stopped", qos=1);   


client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)

client.on_connect = on_connect
client.on_message = on_message

client.loop_forever()
