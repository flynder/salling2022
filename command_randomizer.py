from random import randrange
import time
import paho.mqtt.client as mqtt
import sys
sys.path.insert(1, '/home/robot/')
import config

train_runtime = 60
idle = 10
santa_runtime = 60

client = mqtt.Client()
client.username_pw_set(config.login, config.password)
client.connect(config.host,1883,60)
import datetime as dt
h =  dt.datetime.now().hour + 2


while True:
    h =  dt.datetime.now().hour + 2
    if ((h>6) and (h<=23)):
      x=(randrange(4))
      if x == 0:
         print ("train1 on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "train1 on", qos=0);
         client.disconnect();
         time.sleep(train_runtime)
         print ("train1 off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "train1 off", qos=0);
         client.disconnect();
         time.sleep(idle)
      elif x == 1:
         print ("train2 on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "train2 on", qos=0);
         client.disconnect();
         time.sleep(train_runtime)
         print ("train2 off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "train2 off", qos=0);
         client.disconnect();
         time.sleep(idle)
      elif x == 2:
         print ("train3 on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         #client.publish("topic/commands", "train3 on", qos=0);
         client.disconnect();
         time.sleep(train_runtime)
         print ("train3 off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "train3 off", qos=0);
         client.disconnect();
         time.sleep(idle)
      elif x == 3:
         print ("santa on")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "santa on", qos=0);
         client.disconnect();
         time.sleep(santa_runtime)
         print ("santa off")
         client = mqtt.Client()
         client.username_pw_set(config.login, config.password)
         client.connect(config.host,1883,60)
         client.publish("topic/commands", "santa off", qos=0);
         client.disconnect();
         time.sleep(idle)
         
